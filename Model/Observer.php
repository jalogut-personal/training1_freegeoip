<?php
/**
 * Observer.php
 *
 * @category  magento2
 * @package   magento2_
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training1\FreeGeoIp\Model;

class Observer
{
    /**
     * @var \Magento\Framework\App\AreaList
     */
    protected $_areaList;
    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    protected $_remoteAddress;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\AreaList                      $areaList
     * @param \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
     * @param \Psr\Log\LoggerInterface                             $logger
     */
    public function __construct(
        \Magento\Framework\App\AreaList $areaList,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_areaList = $areaList;
        $this->_remoteAddress = $remoteAddress;
        $this->_logger = $logger;
    }

    /**
     * Save country name and code in session
     *
     * @dispatch controller_front_send_response_before
     *
     * @param $observer
     *
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AuthorizationException
     */
    public function visitorInit($observer)
    {
        $visitor = $observer->getVisitor();
        if (null == $visitor->getCountryCodeTraining()) {
            $ip = $this->_remoteAddress->getRemoteAddress(); // IP example: '178.189.99.114'
            if (!$ip) {
                $this->_logger->error('Customer with no IP is accessing the site');
            }
            $json = file_get_contents('https://freegeoip.net/json/'.$ip);
            $json = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($json));
            $result = json_decode($json, true);
            if (!$result || !is_array($result)) {
                $this->_logger->error('No country returned from freegeoip service');
            }
            $visitor->setCountryCodeTraining($result['country_code']);
            $visitor->setCountryNameTraining($result['country_name']);
            $this->_logger->info(sprintf('Customer with IP: %s and Country: %s-%s started a new session on the site',
                $ip, $result['country_name'], $result['country_code']));

        }
    }

}